package sqlite

import (
	"context"
	"database/sql"
	"fmt"
	"sbox/core/audio"

	"github.com/jmoiron/sqlx"
)

type audioRepository struct {
	*sqlx.DB
}

func NewAudioRepository(db *sqlx.DB) audio.AudioRepository {
	return &audioRepository{
		DB: db,
	}
}

func (s *audioRepository) GetByID(ctx context.Context, id int64) (audio.Audio, error) {
	tx := s.MustBegin()
	audioQuery := `SELECT * FROM audio WHERE id = ?1`
	var existentAudio audio.Audio
	err := tx.QueryRowx(audioQuery, id).Scan(existentAudio)
	if err == sql.ErrNoRows {
		return audio.Audio{}, err
	}
	return existentAudio, nil
}

func (s *audioRepository) Create(ctx context.Context, name string, hash string, path string, duration int64) (audio.Audio, error) {
	// Verify that the unique data is not already in the database.
	tx := s.MustBegin()
	uniqueConstraintQuery := `SELECT * FROM audio WHERE name = ?1`
	var existentAudio audio.Audio
	err := tx.QueryRowx(uniqueConstraintQuery, name).Scan(existentAudio)
	if err != nil {
		if err != sql.ErrNoRows {
			tx.Rollback()
			return audio.Audio{}, fmt.Errorf("entity already exists")
		}
	}

	var newAudio audio.Audio
	query := `INSERT INTO audios (name, hash, path, duration) VALUES (?1, ?2, ?3, ?4) RETURNING *`
	err = tx.QueryRowx(query, name, hash, path, duration).Scan(newAudio)
	if err != nil {
		tx.Rollback()
		return audio.Audio{}, fmt.Errorf("could not create entity")
	}
	tx.Commit()

	return newAudio, nil
}
