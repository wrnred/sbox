CREATE TABLE IF NOT EXISTS audio (
    id INTEGER PRIMARY KEY,
    name VARCHAR(250) NOT NULL UNIQUE CHECK (name <> ''), -- not empty string
    hash VARCHAR(250) NOT NULL UNIQUE CHECK (hash <> ''),
    path VARCHAR(250) NOT NULL UNIQUE CHECK (hash <> ''),
    duration INTEGER DEFAULT 0,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

-- Create trigger to update updated_at column on table updates
CREATE TRIGGER IF NOT EXISTS update_updated_at
AFTER UPDATE ON audio 
FOR EACH ROW
BEGIN
    UPDATE audio
    SET updated_at = CURRENT_TIMESTAMP
    WHERE id = OLD.id;
END;
