package html

import (
	"fmt"
	"sbox/core/audio"
	"sbox/infrastructure/http/html/views"

	"github.com/a-h/templ"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func StartServer(address, port string) {
	e := echo.New()

	registerMiddlewares(e)
	registerRoutes(e)

	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%s", address, port)))
}

func registerMiddlewares(e *echo.Echo) {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
}

func registerRoutes(e *echo.Echo) {
	e.GET("/", index)
}

func index(c echo.Context) error {
	return render(c, views.Index([]*audio.Audio{}))
}

func render(ctx echo.Context, cmp templ.Component) error {
	return cmp.Render(ctx.Request().Context(), ctx.Response())
}
