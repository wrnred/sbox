package main

import (
	"os"
	"sbox/infrastructure/http/html"
)

type serverConfig struct {
	address string
	port    string
}

func loadEnv(c *serverConfig) {
	address := os.Getenv("ADDRESS")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	c.address = address
	c.port = port
}

func main() {
	config := serverConfig{}
	loadEnv(&config)

	html.StartServer(config.address, config.port)
}
