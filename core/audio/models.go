package audio

type CreateAudioRequest struct {
	Name string
	Path string
}
type DeleteAudioRequest struct {
	ID int64 `json:"id"`
}

type AudioResponse struct {
	ID       int64  `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Duration int64  `json:"duration,omitempty"`
	Error    string `json:"error,omitempty"`
}
