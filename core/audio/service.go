package audio

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/tcolgate/mp3"
)

type audioService struct {
	repository AudioRepository
}

func NewUserService(repo AudioRepository) AudioService {
	return &audioService{
		repository: repo,
	}
}

func mp3Duration(mp3FilePath string) (int64, error) {
	file, err := os.Open(mp3FilePath)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	decoder := mp3.NewDecoder(file)
	totalDuration := 0.0

	var frame mp3.Frame
	var skipped int
	for {
		if err := decoder.Decode(&frame, &skipped); err != nil {
			if err == io.EOF {
				break
			}
			return 0, err
		}
		totalDuration += frame.Duration().Seconds()
	}

	duration := time.Second * time.Duration(totalDuration)
	return int64(duration.Seconds()), nil
}

func (s *audioService) Create(ctx context.Context, audio CreateAudioRequest) (AudioResponse, error) {
	// Get the duration of the audio file.
	// TODO: add handler for multiple formats.
	duration, err := mp3Duration(audio.Path)
	if err != nil {
		return AudioResponse{ID: 0}, fmt.Errorf("audio file could not be decoded: %w", err)
	}

	// Generate the hash of the file.
	audiof, err := os.Open(audio.Path)
	if err != nil {
		return AudioResponse{ID: 0}, fmt.Errorf("could not open the audio file: %w", err)
	}
	defer audiof.Close()

	hasher := sha256.New()
	if _, err := io.Copy(hasher, audiof); err != nil {
		return AudioResponse{ID: 0}, fmt.Errorf("could not calculate hash fo the file: %w", err)
	}
	audioFileHash := hex.EncodeToString(hasher.Sum(nil))

	createdAudio, err := s.repository.Create(ctx, audio.Name, audioFileHash, audio.Path, duration)
	if err != nil {
		return AudioResponse{ID: 0}, fmt.Errorf("error at user creation: %w", err)
	}

	return AudioResponse{ID: createdAudio.ID, Name: createdAudio.Name, Duration: createdAudio.Duration}, nil
}

func (s *audioService) GetByID(ctx context.Context, id int64) (AudioResponse, error) {
	audio, err := s.repository.GetByID(ctx, id)
	if err != nil {
		return AudioResponse{}, fmt.Errorf("could not retrieve the requested file: %w", err)
	}

	return AudioResponse{ID: audio.ID, Name: audio.Name, Duration: audio.Duration}, nil
}

//func Delete(ctx context.Context, id int64) error
//func GetAll(ctx context.Context) ([]audio.AudioResponse, error)
