package audio

import (
	"context"
)

type AudioRepository interface {
	Create(ctx context.Context, name string, hash string, path string, duration int64) (Audio, error)
	GetByID(ctx context.Context, id int64) (Audio, error)
	//Delete(ctx context.Context, id int64) error
	//GetAll(ctx context.Context) ([]entities.Audio, error)
}

type AudioService interface {
	Create(ctx context.Context, user CreateAudioRequest) (AudioResponse, error)
	GetByID(ctx context.Context, id int64) (AudioResponse, error)
	//Delete(ctx context.Context, id int64) error
	//GetAll(ctx context.Context) ([]models.AudioResponse, error)
}
