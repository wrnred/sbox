package audio

import "time"

type Audio struct {
	ID        int64     `db:"id"`
	Name      string    `db:"name"`
	Duration  int64     `db:"duration"`
	Hash      string    `db:"hash"`
	Path      string    `db:"path"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}
